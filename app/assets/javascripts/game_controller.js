turn = 'player'
var onDragStart = function(source, piece, position, orientation) {
  if(c.in_checkmate()){
    alert("checkmate")
  }
  if (piece.search(/^b/) !== -1)  {
    return false;
  }
};
var makeComputerMove = function(board_state){
  if(c.in_checkmate()){
    alert("checkmate")
  }
  if(c.in_check() ){
    alert("Em check!")
  }
  $.ajax({
    url: "/game/computer_move",
    type: "POST",
    data: {board_state: board_state},
    success: function (data) {
      if(c.in_check()){
        m = c.moves()[0] 
        mov = c.move(m)
        board1.position(c.fen())
        turn = 'player'
      }
      else{
        from = data["move"][0]
        to = data["move"][1]
        ai_move = `${from}-${to}`
        d = c.move({from:from, to:to})
        if(d==null){
          ri = Math.floor(Math.random() * c.moves().length);
          console.log(d)
          m  = c.moves()[ri]
          c.move(m) 
          board1.position(c.fen())
          turn = 'player'
        }
        else{
          ai_move = `${from}-${to}`
          board1.move(ai_move)
          turn = 'player'
        }
      }
    }
  });
}
var onDrop = function(source, target, piece, newPos, oldPos) {
  if(turn !='player'){
    return 'snapback'
  }
  t = c.move({from:source, to:target})
  if(t==null){
    console.log(source)
    console.log(target)
    return "snapback"
  }
  else{
    $.ajax({
      url: "/game/move",
      type: "POST",
      data: {source: source, target: target, piece: piece, newPos: newPos, oldPos: oldPos},
      success: function (data) {
        move_attempt = `${source}-${target}`
        if(data["valid"]) {
          board1.move(move_attempt);
          turn = 'computer'
          makeComputerMove(newPos);
        }
      }
    });
  }
   $.ajax({
      url: "/game/computer_move",
    type: "POST",
    data: {board_state: board_state},
    success: function (data) {
      if(c.in_check()){
        m = c.moves()[0] 
        mov = c.move(m)
        board1.position(c.fen())
        turn = 'player'
      }
      else{
        from = data["move"][0]
        to = data["move"][1]
        ai_move = `${from}-${to}`
        d = c.move({from:from, to:to})
        if(d==null){
          ri = Math.floor(Math.random() * c.moves().length);
          console.log(d)
          m  = c.moves()[ri]
          c.move(m) 
          board1.position(c.fen())
          turn = 'player'
        }
        else{
          ai_move = `${from}-${to}`
          board1.move(ai_move)
          turn = 'player'
        }
      }
    }
  });
}
var onDrop = function(source, target, piece, newPos, oldPos) {
  if(turn !='player'){
    return 'snapback'
  }
  t = c.move({from:source, to:target})
  if(t==null){
    console.log(source)
    console.log(target)
    return "snapback"
  }
  else{
    $.ajax({
      url: "/game/move",
      type: "POST",
      data: {source: source, target: target, piece: piece, newPos: newPos, oldPos: oldPos},
      success: function (data) {
        move_attempt = `${source}-${target}`
        if(data["valid"]) {
          board1.move(move_attempt);
          turn = 'computer'
          makeComputerMove(newPos);
        }
      }
    });
  }
  return 'snapback'
};

function updateBoardStatus(){
  var target = document.querySelector('.square-55d63')
  // create an observer instance
  var observer = new MutationObserver(function(mutations) {
  });
  // configuration of the observer:
  var config = { attributes: true, childList: true, characterData: true };
  // pass in the target node, as well as the observer options
  observer.observe(target, config);
}
var onMouseoutSquare = function(square, piece) {
  removeGreySquares();
};
var onMouseoutSquare = function(square, piece) {
  removeGreySquares();
};

$( document ).on('turbolinks:load', function() {
  var cfg = {
    draggable: true,
    dropOffBoard: 'snapback',
    position: 'start',
    onDragStart: onDragStart,
    moveSpeed: 'fast',
    snapbackSpeed: 0,
    onDrop: onDrop
  };
  board1 = ChessBoard('board1',cfg);
  c = new Chess()
  c = new Chess()
  updateBoardStatus();
})
