class Bishop < Piece
  include Slideable
  attr_accessor :board, :x, :y, :color
  attr_reader :moves

  MOVES = [[1, 1], [1, -1], [-1, 1], [-1, -1]] 

  def initialize(board, x, y, color)
    @board = board
    @x = x
    @y = y
    @color = color[0]
    @moves = MOVES 
    super
  end

  def value
    score = @color == 'w'?  30 : -30
  end

  def bishop_eval_white
    [
      [ -2.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -2.0],
      [ -1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -1.0],
      [ -1.0,  0.0,  0.5,  1.0,  1.0,  0.5,  0.0, -1.0],
      [ -1.0,  0.5,  0.5,  1.0,  1.0,  0.5,  0.5, -1.0],
      [ -1.0,  0.0,  1.0,  1.0,  1.0,  1.0,  0.0, -1.0],
      [ -1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0, -1.0],
      [ -1.0,  0.5,  0.0,  0.0,  0.0,  0.0,  0.5, -1.0],
      [ -2.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -2.0]
    ]
  end

  def bishop_eval_black
    bishop_eval_white.reverse
  end

end
