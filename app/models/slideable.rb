module Slideable
  include GameHelper

  def valid_moves
    @possible_moves = []

    @moves.each do |move|
      temp_x = @x + move[0]
      temp_y = @y + move[1]
      chess_board_x = inverse_coord_mapping[temp_x]
      chess_board_coord = "#{chess_board_x}#{temp_y}"

      loop do
        break if invalid_coords(temp_x,temp_y)
        chess_board_x = inverse_coord_mapping[temp_x]
        chess_board_coord = "#{temp_x}#{temp_y}"
#        binding.pry
        @possible_moves << [temp_x, temp_y] if board[chess_board_coord].nil? || board[chess_board_coord][0] != @color
        break if !board[chess_board_coord].nil?
        temp_x += move[0]
        temp_y += move[1]
      end
    end
    convert_to_chess_notation @possible_moves
  end
  def valid_moves_computer
    possible_moves = []

    @moves.each do |move|
      temp_x = @x + move[0]
      temp_y = @y + move[1]
      chess_board_x = inverse_coord_mapping[temp_x]
      chess_board_coord = "#{chess_board_x}#{temp_y}"

      loop do
        break if invalid_coords(temp_x,temp_y)
        chess_board_x = inverse_coord_mapping[temp_x]
        chess_board_coord = "#{temp_x}#{temp_y}"
#        binding.pry
        possible_moves << [temp_x, temp_y] if board[chess_board_coord].nil? || board[chess_board_coord][0] != @color
        break if !board[chess_board_coord].nil?
        temp_x += move[0]
        temp_y += move[1]
      end
    end
    convert_to_chess_notation possible_moves
  end
end
