class PieceMover
  attr_accessor :piece, :piece_name
  def initialize(piece_name)
    @piece_name = piece_name
    @piece = piece_class
  end


  def piece_class
    case piece_name[0]
    when 'K'
      'King'.constantize
    when 'N'
      'Knight'.constantize
    when 'B'
      'Bishop'.constantize
    when 'Q'
      'Queen'.constantize
    when 'P'
      'Pawn'.constantize
    when 'R'
      'Rook'.constantize
    end
  end

end
