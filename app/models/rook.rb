class Rook < Piece
  include Slideable
  attr_accessor :board, :x, :y, :color
  attr_reader :moves

  MOVES = [ [0, 1], [0, -1], [1, 0], [-1, 0] ]
  def value
    @color == 'w'? 50:-50
  end

  def initialize(board, x, y, color)
    @board = board
    @x = x
    @y = y
    @color = color[0]
    @moves = MOVES 
    super
  end
  def value
    score = @color == 'w'?  50 : -50
  end

  def rook_eval_white
    [
      [  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0],
      [  0.5,  1.0,  1.0,  1.0,  1.0,  1.0,  1.0,  0.5],
      [ -0.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -0.5],
      [ -0.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -0.5],
      [ -0.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -0.5],
      [ -0.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -0.5],
      [ -0.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -0.5],
      [  0.0,   0.0, 0.0,  0.5,  0.5,  0.0,  0.0,  0.0]
    ]
  end
  def rook_eval_black
    rook_eval_white.reverse
  end



end
