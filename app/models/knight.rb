class Knight < Piece
  include GameHelper
  attr_accessor :board, :x, :y, :color
  attr_reader :moves

  MOVES = [ [-1, -2] , [-2, -1], [-2, 1], [-1, 2], [1, -2], [2, -1], [2, 1], [1, 2] ]
  def initialize(board, x, y, color)
    @board = board
    @x = x
    @y = y
    @color = color
    @moves = MOVES 
    super
  end

  def value
    score = @color == 'w'?  30 : -30
  end

  def knight_eval_white
    [
      [ -3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
      [ -3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
      [ -3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
      [ -3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
      [ -2.0, -3.0, -3.0, -4.0, -4.0, -3.0, -3.0, -2.0],
      [ -1.0, -2.0, -2.0, -2.0, -2.0, -2.0, -2.0, -1.0],
      [  2.0,  2.0,  0.0,  0.0,  0.0,  0.0,  2.0,  2.0 ],
      [  2.0,  3.0,  1.0,  0.0,  0.0,  1.0,  3.0,  2.0 ]]
  end

  def knight_eval_black
    knight_eval_white.reverse
  end

  def valid_moves
    possible_moves = []
    moves.each do |v|
      temp_x = x + v[0]
      temp_y = y + v[1]
      possible_moves << [temp_x,temp_y] unless invalid_coords(temp_x,temp_y)
    end
    convert_to_chess_notation possible_moves
  end

  def valid_moves_computer
    possible_moves = []
    moves.each do |v|
      temp_x = x + v[0]
      temp_y = y + v[1]
      possible_moves << [temp_x,temp_y] unless invalid_coords(temp_x,temp_y)
    end
    convert_to_chess_notation possible_moves
  end

end
