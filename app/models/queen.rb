class Queen < Piece
  include Slideable
  attr_accessor :board, :x, :y, :color
  attr_reader :moves

  MOVES = [ [1, 1], [1, -1], [-1, 1], [-1, -1], [0, 1], [0, -1], [1, 0], [-1, 0]] 

  def initialize(board, x, y, color)
    @board = board
    @x = x
    @y = y
    @color = color[0]
    @moves = MOVES 
    super

  end
  def value
    score = @color == 'w'?  90 : -90
  end

  def queen_eval_white
    [
    [ -2.0, -1.0, -1.0, -0.5, -0.5, -1.0, -1.0, -2.0],
      [ -1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -1.0],
      [ -1.0,  0.0,  0.5,  0.5,  0.5,  0.5,  0.0, -1.0],
      [ -0.5,  0.0,  0.5,  0.5,  0.5,  0.5,  0.0, -0.5],
      [  0.0,  0.0,  0.5,  0.5,  0.5,  0.5,  0.0, -0.5],
      [ -1.0,  0.5,  0.5,  0.5,  0.5,  0.5,  0.0, -1.0],
      [ -1.0,  0.0,  0.5,  0.0,  0.0,  0.0,  0.0, -1.0],
      [ -2.0, -1.0, -1.0, -0.5, -0.5, -1.0, -1.0, -2.0]
    ]
  end

  def queen_eval_black
    queen_eval_white.reverse
  end


end
