class King < Piece
  include GameHelper
  attr_accessor :board, :x, :y, :color
  MOVES = [ [1, 1], [1, 0], [1, -1], [0, 1], [0, -1], [-1, 1], [-1, 0], [-1, -1]]

  def initialize(board, x, y, color)
    @board = board
    @x = x
    @y = y
    @color = color[0]
    @moves = MOVES 
    super
  end

  def value
    score = @color == 'w'?  900 : -900
  end

  def king_eval_white
    [
      [ -3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
      [ -3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
      [ -3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
      [ -3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
      [ -2.0, -3.0, -3.0, -4.0, -4.0, -3.0, -3.0, -2.0],
      [ -1.0, -2.0, -2.0, -2.0, -2.0, -2.0, -2.0, -1.0],
      [  2.0,  2.0,  0.0,  0.0,  0.0,  0.0,  2.0,  2.0 ],
      [  2.0,  3.0,  1.0,  0.0,  0.0,  1.0,  3.0,  2.0 ]
    ]
  end

  def king_eval_black
    king_eval_white.reverse
  end

  def in_check?(positions)
    in_check = false
    positions.flatten.select { |piece| !piece.nil? && piece.color != @color }.each do |piece|
      if piece.instance_of? Pawn
        piece.possible_moves.each do |move|
          in_check = true if move[1] != piece.y && move == [@x, @y]
        end
      else
        in_check = true if piece.possible_moves.include?([@x, @y])
      end
    end

    in_check
  end

  def valid_moves_computer
    possible_moves = []
    moves.each do |v|
      temp_x = x+ v[0]
      temp_y = y + v[1]
      chess_board_coord = "#{temp_x}#{temp_y}"
      next if invalid_coords(temp_x,temp_y) 
      possible_moves << [temp_x,temp_y] if board[chess_board_coord].nil? || board[chess_board_coord][0] != @color
    end
    convert_to_chess_notation possible_moves
  end
  def in_check?(board)
    in_check = false
    positions.select { |k,v| !v.nil? && v[0] != @color }.each do |k,v|
      if v[1] == 'P'
        validMoves = PieceMover.new(v[1]).piece_class.new(board,k[0].to_i,k[1].to_i, v[0]).valid_moves_computer
        validMoves.each do |move|
          binding.pry
          in_check = true if move[1] != piece.y && move == [@x, @y]
        end
      else
        in_check = true if piece.possible_moves.include?([@x, @y])
      end
    end

    in_check
  end

  def valid_moves
    possible_moves = []
    moves.each do |v|
      temp_x = x+ v[0]
      temp_y = y + v[1]
      chess_board_coord = "#{temp_x}#{temp_y}"
      next if invalid_coords(temp_x,temp_y) 
      possible_moves << [temp_x,temp_y] if board[chess_board_coord].nil? || board[chess_board_coord][0] != @color
    end
    convert_to_chess_notation possible_moves
  end
end
