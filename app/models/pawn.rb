class Pawn < Piece
  include GameHelper
  attr_accessor :board, :x, :y, :color
  MOVES = {
    one_step: [0, 1],
    double_step: [0, 2],
    right_diagonal: [1, 1],
    left_diagonal: [-1, 1]
  }

  def initialize(board, x, y, color)
    @board = board
    @x = x
    @y = y
    @color = color[0]
    @moves = MOVES 
    super
  end

  def value
    score = @color == 'w'?  10 : -10
  end

  def pawn_eval_white
     [
        [0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0],
        [5.0,  5.0,  5.0,  5.0,  5.0,  5.0,  5.0,  5.0],
        [1.0,  1.0,  2.0,  3.0,  3.0,  2.0,  1.0,  1.0],
        [0.5,  0.5,  1.0,  2.5,  2.5,  1.0,  0.5,  0.5],
        [0.0,  0.0,  0.0,  2.0,  2.0,  0.0,  0.0,  0.0],
        [0.5, -0.5, -1.0,  0.0,  0.0, -1.0, -0.5,  0.5],
        [0.5,  1.0, 1.0,  -2.0, -2.0,  1.0,  1.0,  0.5],
        [0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0]
    ]
  end
  def pawn_eval_black
    pawn_eval_white.reverse
  end


  def valid_moves
    possible_moves ||= []
    plays = offsets.dup
    plays.each do |k,v|
      temp_x = x+ v[0]
      temp_y = y + v[1]
      chess_board_coord = "#{temp_x}#{temp_y}"
      chess_board_coord_frente_branco = "#{temp_x}#{temp_y + 1}"
      chess_board_coord_frente_preto = "#{temp_x}#{temp_y - 1}"
      next if invalid_coords(temp_x,temp_y) 
      if (k == :left_diagonal) || (k == :right_diagonal)
        possible_moves << [temp_x,temp_y] if !board[chess_board_coord].nil? && board[chess_board_coord][0] != @color
      end
      if k == :one_step 
        possible_moves << [temp_x,temp_y] if board[chess_board_coord].nil?
      end
      if k == :double_step 
        if @color == 'w'
          break if !board[chess_board_coord_frente_branco].nil? && (board[chess_board_coord_frente_branco][0] == @color)
        else
          break if !board[chess_board_coord_frente_preto].nil? && (board[chess_board_coord_frente_preto][0] == @color)
        end
        # verifica se os peoes estao nas primeiras fileiras
        possible_moves << [temp_x,temp_y] if y == 2 && color == 'w' && (board[chess_board_coord].nil? || board[chess_board_coord][0] != @color )
        possible_moves << [temp_x,temp_y] if y == 7 && color == 'b' && (board[chess_board_coord].nil? ||  board[chess_board_coord][0] != @color )
      end
    end
    convert_to_chess_notation possible_moves
  end

  def valid_moves_computer
    possible_moves ||= []
    plays = offsets.dup
    plays.each do |k,v|
      temp_x = x+ v[0]
      temp_y = y + v[1]
      chess_board_coord = "#{temp_x}#{temp_y}"
      chess_board_coord_frente_branco = "#{temp_x}#{temp_y + 1}"
      chess_board_coord_frente_preto = "#{temp_x}#{temp_y - 1}"
      next if invalid_coords(temp_x,temp_y) 
      if (k == :left_diagonal) || (k == :right_diagonal)
        possible_moves << [temp_x,temp_y] if !board[chess_board_coord].nil? && board[chess_board_coord][0] != @color
      end
      if k == :one_step 
        possible_moves << [temp_x,temp_y] if board[chess_board_coord].nil?
      end
      if k == :double_step 
        if @color == 'w'
          break if !board[chess_board_coord_frente_branco].nil? && (board[chess_board_coord_frente_branco][0] == @color)
          break if !board[chess_board_coord_frente_branco].nil? && (board[chess_board_coord_frente_branco][0] == @color)
          next if !board[chess_board_coord_frente_branco].nil? && (board[chess_board_coord_frente_branco][0] == @color)
        else
          next if !board[chess_board_coord_frente_preto].nil? && (board[chess_board_coord_frente_preto][0] == @color)
        end
        # verifica se os peoes estao nas primeiras fileiras
        possible_moves << [temp_x,temp_y] if y == 2 && color == 'w' && (board[chess_board_coord].nil? || board[chess_board_coord][0] != @color ) 
        possible_moves << [temp_x,temp_y] if y == 7 && color == 'b' && (board[chess_board_coord].nil? ||  board[chess_board_coord][0] != @color ) && (!board[chess_board_coord_frente_preto].nil? && (board[chess_board_coord_frente_preto][0] == @color))
        possible_moves << [temp_x,temp_y] if y == 2 && color == 'w' && (board[chess_board_coord].nil? || board[chess_board_coord][0] != @color ) 
        possible_moves << [temp_x,temp_y] if y == 7 && color == 'b' && (board[chess_board_coord].nil? ||  board[chess_board_coord][0] != @color ) && (!board[chess_board_coord_frente_preto].nil? && (board[chess_board_coord_frente_preto][0] == @color))
        possible_moves << [temp_x,temp_y] if y == 2 && color == 'w' && (board[chess_board_coord].nil? || board[chess_board_coord][0] != @color )
        possible_moves << [temp_x,temp_y] if y == 7 && color == 'b' && (board[chess_board_coord].nil? ||  board[chess_board_coord][0] != @color )
      end
    end
    convert_to_chess_notation possible_moves
  end

end
