class Piece
  attr_accessor :board, :x, :y, :color, :moves

  MAPPING =  {1=>"a", 2=>"b", 3=>"c", 4=>"d", 5=>"e", 6=>"f", 7=>"g", 8=>"h"} 

  def initialize(board, x, y, color)
    @board = board
    @x = x
    @y = y
    @color = color[0]
  end

  private
  def offsets
    m = moves.dup
    if color == 'b'
     m.each{|k,v| v[1] *=-1 }
    end
    if color == 'w'
      m.each{|k,v| v[1] = v[1].abs }
    end
    m
  end

  def convert_to_chess_notation(possible_moves)
    arr = possible_moves.map do |p|
      first_coord = p[0]
      first_coord_chr = MAPPING[first_coord]
      "#{first_coord_chr}#{p[1]}"
    end
  end
end
