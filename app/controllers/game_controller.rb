# Classe principal
class GameController < ApplicationController
  include GameHelper
  skip_before_action :verify_authenticity_token
  def index; end

  def check

  end

  def move
    board, x, y, color = state_params
    piece_name = params[:piece][1]
    game_state_f = board.to_h.transform_keys {|k| "#{coord_mapping[k[0]]}#{k[1]}"}
    piece = PieceMover.new(piece_name)
      .piece_class.new(game_state_f,x,y,color)
    valid = piece.valid_moves.include? params[:target]
    puts "jogada valida #{valid}"
    render json: {valid: valid}
  end

  def computer_move
    move = calc_best_move
    render json: {move: move}
  end


  def calc_best_move
    board = computer_params.to_h
    old = board.dup
    game_moves = generate_black_moves(board).select{|k,v| !v.empty?}
    move_tuples = game_moves.reduce([]) { |acc,(k,v)| v.flatten.each {|p| acc << [k,p]}; acc}
    bestMove = nil;
    bestValue = -9999;
    move_tuples.each do |t|
      board[t.second] = board[t.first]
      board.delete(t.first)
      boardValue = -evaluate_board(board)
      board = old
      if(boardValue > bestValue) 
        bestValue = boardValue;
        bestMove = t
      end
    end
    return bestMove
  end


  def generate_black_moves(state)
    positions = state.select{|k,v| v[0]=='b'}.keys
    game_state_f = state.to_h.transform_keys {|k| "#{coord_mapping[k[0]]}#{k[1]}"}
    validMoves =  game_state_f.select{|_,v|v[0] =='b'}.inject({}) { |h, (k, v)| h.merge(k =>  PieceMover.new(v[1]).piece_class.new(game_state_f,k[0].to_i,k[1].to_i,'black').valid_moves_computer) }.values
    positions.zip(validMoves).to_h
  end
  def generate_white_moves(state)
    positions = state.select{|k,v| v[0]=='w'}.keys
    game_state_f = state.to_h.transform_keys {|k| "#{coord_mapping[k[0]]}#{k[1]}"}
    validMoves =  game_state_f.select{|_,v|v[0] =='w'}.inject({}) { |h, (k, v)| h.merge(k =>  PieceMover.new(v[1]).piece_class.new(game_state_f,k[0].to_i,k[1].to_i,'white').valid_moves_computer) }.values
    positions.zip(validMoves).to_h
  end

  def minmaxroot(depth, board, isMaximizingPlayer)
    board_compact = board.to_h.compact
    new_board = board_compact.dup
    old_board = board_compact.dup
    game_moves = generate_black_moves(board)
    game_moves = generate_black_moves(board)
    game_moves = generate_black_moves(board).merge(generate_white_moves(board))
    bestMove = -9999;
    best_move_found = []
    move_tuples = game_moves.reduce([]) { |acc,(k,v)| v.flatten.each {|p| acc << [k,p]}; acc}
    move_tuples.each do |t|
      new_board[t.second] = new_board[t.first]
      new_board.delete(t.first)
      value = minimax(depth - 1, new_board, !isMaximizingPlayer);
      new_board = old_board.dup
      if(value >= bestMove)
        bestMove = value
        best_move_found = t
      end
      return best_move_found
    end
  end

  def minimax(depth, board, isMaximizingPlayer, old_board = board.dup)
    if(depth == 0) 
      return -evaluate_board(board)
    end
    board = board.to_h.compact
    new_board = old_board
    old_board = board.dup
    game_moves = generate_black_moves(board)
    game_moves = generate_black_moves(board)
    game_moves = generate_black_moves(board).merge(generate_white_moves(board))
    move_tuples = game_moves.reduce([]) { |acc,(k,v)| v.flatten.each {|p| acc << [k,p]}; acc}
    if (isMaximizingPlayer)
      bestMove = -9999;
      move_tuples.each do |t|
        new_board[t.second] = new_board[t.first]
        new_board.delete(t.first)
        bestMove = [bestMove,minimax(depth - 1, new_board, !isMaximizingPlayer, old_board)].max
        new_board = old_board
      end
      return bestMove
    else
      bestMove = 9999;
      move_tuples.each do |t|
        new_board[t.second] = new_board[t.first]
        new_board.delete(t.first)
        bestMove = [bestMove,minimax(depth - 1, new_board, !isMaximizingPlayer, old_board)].min
        new_board = old_board
      end
      return bestMove
    end
  end

  def evaluate_board(board)
    sum = board.to_h.compact.reduce(0) do |acc,(k,v)| 
      color = v[0] 
      p = v[1]
      acc+= PieceMover.new(p).piece_class.new(board,0,0,color).value
    end
  end

  def computer_params
    params.require(:board_state).permit!
  end

  def state_params
    params.permit!
    [ board  = params[:oldPos], x = coord_mapping[params[:source][0]],
      y = params[:source][1].to_i,
      color = params[:piece][0] ]
  end



end
