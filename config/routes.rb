Rails.application.routes.draw do
  root to: 'game#index'
  post 'game/move'
  post 'game/check'
  post 'game/computer_move'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
