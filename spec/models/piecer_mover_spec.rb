require "rails_helper"

RSpec.describe PieceMover, :type => :model do
  describe ".piece_class" do
    it 'retorna a classe correta'  do
      p = PieceMover.new('N')
      klass = p.piece_class
      expect(klass).to eq 'Knight'.constantize
    end
  end
end
