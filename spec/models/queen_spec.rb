require "rails_helper"

RSpec.describe Queen, :type => :model do
  describe ".valid_moves" do
    let (:board) { {"c8"=>"bB", "d8"=>"bQ", "e8"=>"bK", "f8"=>"bB", "a7"=>"bP", "b7"=>"bP", "d7"=>"bP", "e7"=>"bP", "f7"=>"bP", "g7"=>"bP", "h7"=>"bP", "a2"=>"wP", "b2"=>"wP", "c2"=>"wP", "e2"=>"wP", "f2"=>"wP", "g2"=>"wP", "h2"=>"wP", "a1"=>"wR", "b1"=>"wN", "c1"=>"wB", "e1"=>"wK", "f1"=>"wB", "g1"=>"wN", "h1"=>"wR", "d4"=>"wP", "a6"=>"bN", "b8"=>"bR", "f6"=>"bN", "g8"=>"bR", "g4"=>"wQ", "c6"=>"bP"}}

  let(:x) { 7 }
  let(:y) { 4 }
    it 'gera movimentos validos para a Rainha' do
       v_moves = Queen.new(board,3,3,'white').valid_moves
       expect(v_moves).to eq ["d4", "e5", "f6", "g7", "h8", "d2", "e1", "b4", "a5", "b2", "a1", "c4", "c5", "c6", "c7", "c8", "c2", "c1", "d3", "e3", "f3", "g3", "h3", "b3", "a3"]
    end
  end
end
