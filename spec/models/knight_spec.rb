require "rails_helper"

RSpec.describe Knight, :type => :model do
  describe ".valid_moves" do
    let (:board) { {"a8"=>"bR", "b8"=>"bN", "c8"=>"bB", "d8"=>"bQ", "e8"=>"bK", "f8"=>"bB", "g8"=>"bN", "h8"=>"bR", "a7"=>"bP", "b7"=>"bP", "c7"=>"bP", "d7"=>"bP", "e7"=>"bP", "f7"=>"bP", "g7"=>"bP", "h7"=>"bP", "a2"=>"wP", "b2"=>"wP", "c2"=>"wP", "d2"=>"wP", "e2"=>"wP", "f2"=>"wP", "g2"=>"wP", "h2"=>"wP", "a1"=>"wR", "b1"=>"wN", "c1"=>"wB", "d1"=>"wQ", "e1"=>"wK", "f1"=>"wB", "g1"=>"wN", "h1"=>"wR"} }

  let(:x) { 2 }
  let(:y) { 1 }
    it 'gera movimentos validos para o Cavalo' do
       v_moves = Knight.new(board,2,1,'white').valid_moves
       expect(v_moves).to eq ["a3", "d2", "c3"]
    end
  end
end
