require "rails_helper"

RSpec.describe Bishop, :type => :model do
  describe ".valid_moves" do
    let (:board) { {"c8"=>"bB", "f8"=>"bB", "a7"=>"bP", "b7"=>"bP", "d7"=>"bP", "e7"=>"bP", "f7"=>"bP", "g7"=>"bP", "h7"=>"bP", "a2"=>"wP", "b2"=>"wP", "c2"=>"wP", "e2"=>"wP", "f2"=>"wP", "g2"=>"wP", "h2"=>"wP", "a1"=>"wR", "b1"=>"wN", "e1"=>"wK", "f1"=>"wB", "g1"=>"wN", "h1"=>"wR", "d4"=>"wP", "a6"=>"bN", "b8"=>"bR", "f6"=>"bN", "g8"=>"bR", "c6"=>"bP", "f4"=>"wQ", "c7"=>"bQ", "d2"=>"wB", "d8"=>"bK"}}

  let(:x) { 4 }
  let(:y) { 2 }
    it 'gera movimentos validos para o Bishop' do
       v_moves = Bishop.new(board,x,y,'white').valid_moves
       expect(v_moves).to eq ["e3", "f4", "g5", "h6", "e1", "c3", "b4", "a5", "c1"]
    end
  end
end
