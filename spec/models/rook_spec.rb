require "rails_helper"

RSpec.describe Rook, :type => :model do
  describe ".valid_moves" do
    let (:board) { {"c8"=>"bB", "d8"=>"bQ", "e8"=>"bK", "f8"=>"bB", "h8"=>"bR", "a7"=>"bP", "b7"=>"bP", "c7"=>"bP", "d7"=>"bP", "e7"=>"bP", "f7"=>"bP", "g7"=>"bP", "h7"=>"bP", "c2"=>"wP", "d2"=>"wP", "e2"=>"wP", "f2"=>"wP", "g2"=>"wP", "h2"=>"wP", "b1"=>"wN", "c1"=>"wB", "d1"=>"wQ", "e1"=>"wK", "f1"=>"wB", "g1"=>"wN", "h1"=>"wR", "b4"=>"bN", "a4"=>"wP", "b8"=>"bR", "f6"=>"bN", "c3"=>"wR"}}

  let(:x) { 3 }
  let(:y) { 3 }
    it 'gera movimentos validos para o Rook' do
       v_moves = Rook.new(board,3,3,'white').valid_moves
       expect(v_moves).to eq ["c4", "c5", "c6", "c7", "c8", "c2", "c1", "d3", "e3", "f3", "g3", "h3", "b3", "a3"]
    end
  end
end
